package kr.ac.mju.mp2019f.musicplayer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView mp1_cover;
    private TextView mp1_artist;
    private TextView mp1_title;
    private TextView mp1_play_num;
    private TextView mp1_likes_num;

    private ImageView mp2_cover;
    private TextView mp2_artist;
    private TextView mp2_title;
    private TextView mp2_play_num;
    private TextView mp2_likes_num;

    private SharedPreferences myPrefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mp1_cover = findViewById(R.id.mp1_cover);
        mp1_artist = findViewById(R.id.mp1_artist_tv);
        mp1_title = findViewById(R.id.mp1_title_tv);
        mp1_play_num = findViewById(R.id.mp1_playnum_tv);
        mp1_likes_num = findViewById(R.id.mp1_likesnum_tv);

        mp2_cover = findViewById(R.id.mp2_cover);
        mp2_artist = findViewById(R.id.mp2_artist_tv);
        mp2_title = findViewById(R.id.mp2_title_tv);
        mp2_play_num = findViewById(R.id.mp2_playnum_tv);
        mp2_likes_num = findViewById(R.id.mp2_likesnum_tv);

        myPrefs = getSharedPreferences("main",MODE_PRIVATE);

        String str_mp1_play = myPrefs.getString("mp1_play","0");
        String str_mp1_like = myPrefs.getString("mp1_like","0");

        mp1_artist.setText("Bethesda Fallout 76");
        mp1_title.setText("Take me Home, Country Roads");
        mp1_play_num.setText(str_mp1_play);
        mp1_likes_num.setText(str_mp1_like);

        String str_mp2_play = myPrefs.getString("mp2_play","0");
        String str_mp2_like = myPrefs.getString("mp2_like","0");

        mp2_artist.setText("Fallout 4 - The Five Stars");
        mp2_title.setText("Atom Bomb Baby");
        mp2_play_num.setText(str_mp2_play);
        mp2_likes_num.setText(str_mp2_like);


        mp1_cover.setOnClickListener(this);
        mp2_cover.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        Intent intent = new Intent(this,MusicActivity.class);

        switch (view.getId()){
            case R.id.mp1_cover:


                String str_mp1_artist = mp1_artist.getText().toString();
                String str_mp1_title = mp1_title.getText().toString();
                String str_mp1_play = mp1_play_num.getText().toString();
                String str_mp1_likes = mp1_likes_num.getText().toString();

                Bitmap sendCover1 = BitmapFactory.decodeResource(getResources(),R.drawable.fallout76);
                ByteArrayOutputStream musicCover1 = new ByteArrayOutputStream();
                sendCover1.compress(Bitmap.CompressFormat.PNG, 100, musicCover1);
                byte[] byteArray1 = musicCover1.toByteArray();
                intent.putExtra("cover",byteArray1);

                intent.putExtra("artist",str_mp1_artist);
                intent.putExtra("title",str_mp1_title);

                intent.putExtra("plays",str_mp1_play);
                intent.putExtra("likes",str_mp1_likes);


                startActivityForResult(intent,76);

                break;

            case R.id.mp2_cover:

                String str_mp2_artist = mp2_artist.getText().toString();
                String str_mp2_title = mp2_title.getText().toString();
                String str_mp2_play = mp2_play_num.getText().toString();
                String str_mp2_likes = mp2_likes_num.getText().toString();

                Bitmap sendCover2 = BitmapFactory.decodeResource(getResources(),R.drawable.fallout4);
                ByteArrayOutputStream musicCover2 = new ByteArrayOutputStream();
                sendCover2.compress(Bitmap.CompressFormat.PNG, 100, musicCover2);
                byte[] byteArray2 = musicCover2.toByteArray();
                intent.putExtra("cover",byteArray2);

                intent.putExtra("artist",str_mp2_artist);
                intent.putExtra("title",str_mp2_title);

                intent.putExtra("plays",str_mp2_play);
                intent.putExtra("likes",str_mp2_likes);

                startActivityForResult(intent,4);

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 5000){
            switch(requestCode){
                case 76:

                    mp1_play_num.setText(data.getStringExtra("mpplay"));
                    mp1_likes_num.setText(data.getStringExtra("mplikes"));


                    break;
                case 4:

                    mp2_play_num.setText(data.getStringExtra("mpplay"));
                    mp2_likes_num.setText(data.getStringExtra("mplikes"));

                    break;
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        myPrefs = getSharedPreferences("main",MODE_PRIVATE);
        SharedPreferences.Editor editor = myPrefs.edit();

        String mp1_Play = mp1_play_num.getText().toString();
        editor.putString("mp1_play",mp1_Play);

        String mp1_Like = mp1_likes_num.getText().toString();
        editor.putString("mp1_like",mp1_Like);

        String mp2_Play = mp2_play_num.getText().toString();
        editor.putString("mp2_play",mp2_Play);

        String mp2_Like = mp2_likes_num.getText().toString();
        editor.putString("mp2_like",mp2_Like);

        editor.commit();
    }
}
