package kr.ac.mju.mp2019f.musicplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MusicActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView mp_cover;
    private TextView mp_artist;
    private TextView mp_title;

    private SeekBar seekBar;
    private TextView mp_current_time;
    private TextView mp_left_time;

    private Button play_paues_btn;
    private TextView play_sum;

    private Button stop_btn;

    private Button like_btn;
    private TextView mp_like_num;

    //Music and Time;
    private MediaPlayer mp;
    private SimpleDateFormat df;

    private int current_time;
    private int left_time;

    private int time_setting;


    private SharedPreferences mpPrefs;


    private boolean play_check = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);

        mp_cover = findViewById(R.id.mp_cover);
        mp_artist = findViewById(R.id.mp_artist_name);
        mp_title = findViewById(R.id.mp_title_name);

        seekBar = findViewById(R.id.seekBar);
        mp_current_time = findViewById(R.id.mp_current_time);
        mp_left_time = findViewById(R.id.mp_left_time);

        play_paues_btn = findViewById(R.id.mp_pause_bt);
        play_sum = findViewById(R.id.mp_play_num);
        stop_btn = findViewById(R.id.mp_stop_bt);

        like_btn = findViewById(R.id.mp_like_bt);
        mp_like_num = findViewById(R.id.mp_like_num);

        df = new SimpleDateFormat("mm:ss");

        play_paues_btn.setOnClickListener(this);
        stop_btn.setOnClickListener(this);
        like_btn.setOnClickListener(this);



        final Handler mHandler = new Handler();

        Bundle extras = getIntent().getExtras();
        String music_title = extras.getString("title");


        if(music_title.equals("Take me Home, Country Roads")){

            mpPrefs = getSharedPreferences("music_1",MODE_PRIVATE);

            byte[] arr = getIntent().getByteArrayExtra("cover");
            Bitmap bitmap = BitmapFactory.decodeByteArray(arr,0,arr.length);

            mp_cover.setImageBitmap(bitmap);

            mp_artist.setText(extras.getString("artist"));
            mp_title.setText(extras.getString("title"));


            //플레이 및 좋아요 확인용

            String str_nLike = mpPrefs.getString("nLike_1",extras.getString("likes"));
            mp_like_num.setText(str_nLike);

            String str_nPlay = mpPrefs.getString("nPlay_1",extras.getString("plays"));
            play_sum.setText(str_nPlay);


            //mp3 시크바 세팅 및 노래 재생 세팅
            mp = MediaPlayer.create(getApplicationContext(),R.raw.countryroad);

            seekBar.setMax(mp.getDuration());

            current_time = mpPrefs.getInt("cTime_1",0);

            left_time = mp.getDuration();

            seekBar.setProgress(current_time);
            mp_current_time.setText(df.format(new Date(current_time)));

            mp.seekTo(current_time);

            MusicActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(mp != null && mp.isPlaying()){
                        seekBar.setProgress(mp.getCurrentPosition());
                        current_time = mp.getCurrentPosition();
                        left_time = mp.getDuration() - current_time;

                        mp_current_time.setText(df.format(new Date(current_time)));
                    }

                    mp_left_time.setText(df.format(new Date(left_time)));
                    mHandler.postDelayed(this,1000);
                }
            });

            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    if(b && mp != null){
                        mp.seekTo(i);
                        mp_current_time.setText(df.format(new Date(i)));

                        left_time = mp.getDuration() - i;
                        mp_left_time.setText(df.format(new Date(left_time)));
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

        }else if(music_title.equals("Atom Bomb Baby")){

            mpPrefs = getSharedPreferences("music_2",MODE_PRIVATE);

            byte[] arr = getIntent().getByteArrayExtra("cover");
            Bitmap bitmap = BitmapFactory.decodeByteArray(arr,0,arr.length);

            mp_cover.setImageBitmap(bitmap);

            mp_artist.setText(extras.getString("artist"));
            mp_title.setText(extras.getString("title"));
            mp_like_num.setText(extras.getString("likes"));
            play_sum.setText(extras.getString("plays"));

            mp = MediaPlayer.create(getApplicationContext(),R.raw.atomboy);

            current_time = mpPrefs.getInt("cTime_2",0);

            seekBar.setMax(mp.getDuration());

            left_time = mp.getDuration();

            seekBar.setProgress(current_time);
            mp_current_time.setText(df.format(new Date(current_time)));

            mp.seekTo(current_time);

            MusicActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(mp != null && mp.isPlaying()){
                        seekBar.setProgress(mp.getCurrentPosition());
                        current_time = mp.getCurrentPosition();
                        left_time = mp.getDuration() - current_time;

                        mp_current_time.setText(df.format(new Date(current_time)));
                    }

                    mp_left_time.setText(df.format(new Date(left_time)));
                    mHandler.postDelayed(this,1000);
                }
            });

            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    if(b && mp != null){
                        mp.seekTo(i);
                        mp_current_time.setText(df.format(new Date(i)));

                        left_time = mp.getDuration() - i;
                        mp_left_time.setText(df.format(new Date(left_time)));
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }
    }

    @Override
    public void onClick(View view) {

        Intent mpIntent = new Intent();

        String str_play = play_sum.getText().toString();
        String str_likes = mp_like_num.getText().toString();



        switch (view.getId()){

            case R.id.mp_pause_bt:

                if(play_check != true){
                    play_check = true;

                    int num_play = Integer.parseInt(str_play);
                    num_play = num_play + 1;
                    str_play = Integer.toString(num_play);
                    play_sum.setText(str_play);

                    if(mp == null)return;

                    if(mp.isPlaying()){

                    }else{
                        mp.start();
                        play_paues_btn.setBackgroundResource(R.drawable.pause);
                    }
                }else{
                    if(mp == null)return;

                    if(mp.isPlaying()){
                        mp.pause();
                        play_paues_btn.setBackgroundResource(R.drawable.play);

                    }else{
                        mp.start();
                        play_paues_btn.setBackgroundResource(R.drawable.pause);
                    }
                }

                break;

            case R.id.mp_stop_bt:
                if (mp == null)return;

                if (mp.isPlaying()){
                    mp.pause();
                    play_paues_btn.setBackgroundResource(R.drawable.play);

                    current_time = 0;
                    left_time = mp.getDuration();

                    mp_current_time.setText(df.format(new Date(current_time)));
                    mp_left_time.setText(df.format(new Date(left_time)));
                    seekBar.setProgress(current_time);
                    mp.seekTo(current_time);
                }else{
                    current_time = 0;
                    left_time = mp.getDuration();

                    mp_current_time.setText(df.format(new Date(current_time)));
                    mp_left_time.setText(df.format(new Date(left_time)));
                    seekBar.setProgress(current_time);
                    mp.seekTo(current_time);
                }
                break;

            case R.id.mp_like_bt:
                int num_like = Integer.parseInt(str_likes);
                num_like = num_like + 1;
                str_likes = Integer.toString(num_like);
                mp_like_num.setText(str_likes);

                break;
        }

        mpIntent.putExtra("mpplay",str_play);
        mpIntent.putExtra("mplikes",str_likes);

        setResult(5000,mpIntent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        play_check = false;
        mp.stop();

        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mp_title.getText().toString().equals("Take me Home, Country Roads") == true){
            mpPrefs = getSharedPreferences("music_1",MODE_PRIVATE);

            SharedPreferences.Editor editor = mpPrefs.edit();

            String mp_Play = play_sum.getText().toString();
            editor.putString("nPlay_1",mp_Play);

            int mp_Current = current_time;
            editor.putInt("cTime_1",mp_Current);

            String mp_Like = mp_like_num.getText().toString();
            editor.putString("nLike_1",mp_Like);

            editor.commit();
        }else if(mp_title.getText().toString().equals("Atom Bomb Baby") == true){
            mpPrefs = getSharedPreferences("music_2",MODE_PRIVATE);

            SharedPreferences.Editor editor = mpPrefs.edit();

            int mp_Current = current_time;
            editor.putInt("cTime_2",mp_Current);

            editor.commit();
        }
    }
}
